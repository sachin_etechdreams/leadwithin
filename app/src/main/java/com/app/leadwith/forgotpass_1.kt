package com.app.leadwith

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_forgotpass_1.*

class forgotpass_1 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgotpass_1)

        button.setOnClickListener {

            val intent = Intent(this,forgotpass_2::class.java)
            startActivity(intent)
        }
    }
}
