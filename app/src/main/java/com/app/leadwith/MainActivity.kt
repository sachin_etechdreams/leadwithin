package com.app.leadwith

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main1.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main1)

        val imageView2 = findViewById(R.id.imageView2) as ImageView
        imageView2.setOnClickListener {
            val intent = Intent(this,appbar::class.java)
            startActivity(intent)
        }

        val textlog1 = findViewById(R.id.textlog1) as TextView
        textlog1.setOnClickListener {
            val intent = Intent(this,LoginActivity::class.java)
            startActivity(intent)
        }


        btnLog.setOnClickListener {

            val intent = Intent(this,signup_1::class.java)
            startActivity(intent)
        }
    }
}
