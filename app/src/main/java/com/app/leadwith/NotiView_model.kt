package com.app.leadwith

class NotiView_model {

    var names: String? = null
    var image_source1: Int = 0

    fun getName(): String{
        return names.toString()
    }

    fun setName(names: String){
        this.names = names
    }

    fun  getImages(): Int{
        return image_source1
    }

    fun setImages(image_source1: Int){
        this.image_source1 = image_source1
    }
}