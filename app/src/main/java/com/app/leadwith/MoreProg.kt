package com.app.leadwith

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView

class MoreProg : AppCompatActivity() {

    private var listView: ListView? = null
    private var cAdapter: prog_adapter? = null
    private var imageArrayList: ArrayList<prog_model>? = null

    private var progImage = intArrayOf(R.drawable.prog1, R.drawable.prog2, R.drawable.prog3)
    private var progName = arrayOf("Let's Buildup", "Define", "Let's Level Up")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_more_prog)

        listView = findViewById(R.id.list_prog) as ListView
        imageArrayList = fun_PopulateList()

        cAdapter = prog_adapter(this,imageArrayList!!)
        listView!!.adapter = cAdapter
    }

    private fun fun_PopulateList(): ArrayList<prog_model>{
        val list = java.util.ArrayList<prog_model> ()

        for(i in 0..2){
            val imageModel = prog_model()
            imageModel.setImage(progImage[i])
            imageModel.setNames(progName[i])
            list.add(imageModel)
        }
        return list
    }
}
