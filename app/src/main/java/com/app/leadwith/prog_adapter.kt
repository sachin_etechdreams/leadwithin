package com.app.leadwith

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class prog_adapter

    (private val context: Context,
    private val ListViewArrayList: ArrayList<prog_model>): BaseAdapter(){

        override fun getViewTypeCount(): Int{
        return count
    }

        override fun getItemViewType(position: Int): Int {
            return position
        }

        override fun getCount(): Int {
            return ListViewArrayList.size
        }

        override fun getItem(position: Int): Any {
            return ListViewArrayList[position]
        }

        override fun getItemId(position: Int): Long {
            return 0
        }

        override fun getView(position: Int, itemView: View?, parent: ViewGroup): View {
            var itemView1 = itemView
            val holder: ViewHolder

            if (itemView1 == null) {
                holder = ViewHolder()
                val inflater = context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                itemView1 = inflater.inflate(R.layout.list_view_model, null, true)

                holder.prog1 = itemView1!!.findViewById(R.id.prog1) as TextView
                holder.iv = itemView1.findViewById(R.id.img1) as ImageView

                itemView1.tag = holder
            } else {
                holder = itemView1.tag as ViewHolder
            }

            holder.prog1!!.setText(ListViewArrayList[position].getNames())
            holder.iv!!.setImageResource(ListViewArrayList[position].getImage())


            return itemView1
        }
        private inner class ViewHolder {
            var prog1: TextView? = null
            internal var iv: ImageView? = null
        }
    }
