package com.app.leadwith

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class HomeAdapter : RecyclerView.Adapter<HomeAdapter.ViewHolder>(){


    private val itemTitles = arrayOf("text1","text2","text3","text4","text2","text3","text4")

    private val itemDetails = arrayOf("text1Des", "text2Des","textDes","textDes", "text2Des","textDes","textDes")

    private val itemImages = intArrayOf(
        R.drawable.q1,
        R.drawable.q2,
        R.drawable.q3,
        R.drawable.prog1,
        R.drawable.prog2,
        R.drawable.prog3,
        R.drawable.q1,
        R.drawable.q2
    )


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        var image : ImageView
        var textTitle : TextView
        var textDes : TextView

        init {
            textTitle = itemView.findViewById(R.id.line1)
            textDes = itemView.findViewById(R.id.line2)
            image = itemView.findViewById(R.id.item_image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_recyclerview_model,parent,false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textTitle.text = itemTitles[position]
        holder.textDes.text = itemDetails[position]
        holder.image.setImageResource(itemImages[position])


    }

    override fun getItemCount(): Int {
        return itemTitles.size
    }
}