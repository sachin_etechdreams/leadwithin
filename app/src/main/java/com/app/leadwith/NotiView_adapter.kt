package com.app.leadwith

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView

class NotiView_adapter

    (private val context1: Context,
     private val ListViewArrayList1: ArrayList<NotiView_model>): BaseAdapter(){

    override fun getViewTypeCount(): Int{
        return count
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getCount(): Int {
        return ListViewArrayList1.size
    }

    override fun getItem(position: Int): Any {
        return ListViewArrayList1[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, itemView: View?, parent: ViewGroup): View {
        var itemView1 = itemView
        val holder1: ViewHolder

        if (itemView1 == null) {
            holder1 = ViewHolder()
            val inflater1 = context1
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            itemView1 = inflater1.inflate(R.layout.list_view_model_noti, null, true)

            holder1.noti1 = itemView1!!.findViewById(R.id.noti1) as TextView
            holder1.iv1 = itemView1.findViewById(R.id.img2) as ImageView

            itemView1.tag = holder1
        } else {
            holder1 = itemView1.tag as ViewHolder
        }

        holder1.noti1!!.setText(ListViewArrayList1[position].getName())
        holder1.iv1!!.setImageResource(ListViewArrayList1[position].getImages())


        return itemView1
    }
    private inner class ViewHolder {
        var noti1: TextView? = null
        internal var iv1: ImageView? = null
    }
}
