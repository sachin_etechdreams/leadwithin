package com.app.leadwith

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val textfp = findViewById(R.id.textfp) as TextView
        textfp.setOnClickListener {
            val intent = Intent(this,forgotpass_1::class.java)
            startActivity(intent)
        }

        val textlog3 = findViewById(R.id.textlog3) as TextView
        textlog3.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            startActivity(intent)
        }

        btnLogin.setOnClickListener {
            val intent = Intent(this,Settime::class.java)
            startActivity(intent)
        }
    }
}
