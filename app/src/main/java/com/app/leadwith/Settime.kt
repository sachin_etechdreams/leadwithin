package com.app.leadwith

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_settime.*

class Settime : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settime)

        btnSet.setOnClickListener {
            val intent = Intent(this,begin::class.java)
            startActivity(intent)
        }
    }
}
