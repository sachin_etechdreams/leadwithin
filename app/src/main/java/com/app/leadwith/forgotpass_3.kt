package com.app.leadwith

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_forgotpass_3.*
import kotlinx.android.synthetic.main.activity_signup_4.*

class forgotpass_3 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgotpass_3)

        btnLog4.setOnClickListener {
            val intent = Intent(this,forgotpass_4::class.java)
            startActivity(intent)
        }
    }
}
