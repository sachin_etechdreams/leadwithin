package com.app.leadwith

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_home.*

class home : AppCompatActivity() {

    private var layoutManager:  RecyclerView.LayoutManager? = null
    private var adapter: RecyclerView.Adapter<HomeAdapter.ViewHolder>?= null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager

        adapter = HomeAdapter()
        recyclerView.adapter = adapter
    }

    private fun RecyclerView(): RecyclerView.Adapter<HomeAdapter.ViewHolder>? {
        TODO("Not yet implemented")
    }
}
