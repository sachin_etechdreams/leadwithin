package com.app.leadwith

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView

class Notification : AppCompatActivity() {

    private var listView1: ListView? = null
    private var cAdapter1: NotiView_adapter? = null
    private var imageArrayList1: ArrayList<NotiView_model>? = null

    private var progImage1 = intArrayOf(R.drawable.q1, R.drawable.q2, R.drawable.q3, R.drawable.prog1, R.drawable.prog2, R.drawable.prog3, R.drawable.q2)
    private var progName1 = arrayOf("abc inspired by your post", "abs commented", "xyz inspired by your post","abc inspired by your post",
        "abs commented","abc inspired by your post", "abs commented")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)

        listView1 = findViewById(R.id.list_prog1) as ListView
        imageArrayList1 = fun_PopulateLists()

        cAdapter1 = NotiView_adapter(this,imageArrayList1!!)
        listView1!!.adapter = cAdapter1
    }

    private fun fun_PopulateLists(): ArrayList<NotiView_model>? {
        val list1 = java.util.ArrayList<NotiView_model> ()

        for(i in 0..7){
            val imageModel1 = NotiView_model()
            imageModel1.setImages(progImage1[i])
            imageModel1.setName(progName1[i])
            list1.add(imageModel1)
        }
        return list1
    }
    }

