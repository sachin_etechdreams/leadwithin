package com.app.leadwith

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import kotlinx.android.synthetic.main.activity_begin.*
import kotlinx.android.synthetic.main.activity_settime.*

class begin : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_begin)

        val imageView20 = findViewById(R.id.imageView20) as ImageView
        imageView20.setOnClickListener {
            val intent = Intent(this,MoreProg::class.java)
            startActivity(intent)
        }

        val imageView21 = findViewById(R.id.imageView21) as ImageView
        imageView21.setOnClickListener {
            val intent = Intent(this,About_us::class.java)
            startActivity(intent)
        }

        val imageView22 = findViewById(R.id.imageView22) as ImageView
        imageView22.setOnClickListener {
            val intent = Intent(this,About_app::class.java)
            startActivity(intent)
        }

        val imageView23 = findViewById(R.id.imageView23) as ImageView
        imageView23.setOnClickListener {
            val intent = Intent(this,EditTime::class.java)
            startActivity(intent)
        }

        val imageView24 = findViewById(R.id.imageView24) as ImageView
        imageView24.setOnClickListener {
            val intent = Intent(this,UserProfile::class.java)
            startActivity(intent)
        }

        val imageView25 = findViewById(R.id.imageView25) as ImageView
        imageView25.setOnClickListener {
            val intent = Intent(this,PasswordChange::class.java)
            startActivity(intent)
        }

        butset.setOnClickListener {
            val intent = Intent(this,home::class.java)
            startActivity(intent)
        }
    }
}
