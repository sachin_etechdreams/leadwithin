package com.app.leadwith

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView

class forgotpass_2 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgotpass_2)

        val imagemail = findViewById(R.id.imagemail) as ImageView

        imagemail.setOnClickListener {

            val intent = Intent(this,forgotpass_3::class.java)
            startActivity(intent)
        }
    }
}
