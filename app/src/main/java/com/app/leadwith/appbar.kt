package com.app.leadwith

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import kotlinx.android.synthetic.main.appbar.*

class appbar : AppCompatActivity(){

    lateinit var toggle: ActionBarDrawerToggle
    lateinit var drawerLayout: DrawerLayout
    lateinit var navView: NavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.appbar)

       toggle = ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        navView.setNavigationItemSelectedListener {
            when(it.itemId){
                R.id.menu_home -> Toast.makeText(applicationContext,
                    "Click item 1",Toast.LENGTH_SHORT).show()
                R.id.menu_profile -> Toast.makeText(applicationContext,
                    "Click item 1",Toast.LENGTH_SHORT).show()
                R.id.menu_programs -> Toast.makeText(applicationContext,
                    "Click item 1",Toast.LENGTH_SHORT).show()
                R.id.menu_app -> Toast.makeText(applicationContext,
                    "Click item 1",Toast.LENGTH_SHORT).show()
                R.id.menu_us -> Toast.makeText(applicationContext,
                    "Click item 1",Toast.LENGTH_SHORT).show()
                R.id.menu_time -> Toast.makeText(applicationContext,
                    "Click item 1",Toast.LENGTH_SHORT).show()
                R.id.menu_pass -> Toast.makeText(applicationContext,
                    "Click item 1",Toast.LENGTH_SHORT).show()
                R.id.menu_out -> Toast.makeText(applicationContext,
                    "Click item 1",Toast.LENGTH_SHORT).show()
            }
            true
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (toggle.onOptionsItemSelected(item)){
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}





