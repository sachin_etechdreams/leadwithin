package com.app.leadwith

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


class RecyclerViewAdapter : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>(){

    private val itemTitles = arrayOf("text1", "text2")

    private val itemDetails = arrayOf("text1Des", "text2Des")


    inner class ViewHolder(itemView: View) :    RecyclerView.ViewHolder(itemView){

        var image: ImageView
        var  textTitle: TextView
        var textDes: TextView

        init {
            image = itemView.findViewById(R.id.item_image)
            textTitle = itemView.findViewById(R.id.txt1)
            textDes = itemView.findViewById(R.id.txt2)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_recyclerview_model,parent,false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }

    override fun getItemCount(): Int {
        TODO("Not yet implemented")
    }
}